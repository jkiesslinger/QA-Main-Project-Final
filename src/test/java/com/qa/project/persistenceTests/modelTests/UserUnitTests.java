package com.qa.project.persistenceTests.modelTests;

import com.qa.project.persistence.model.User;
import com.qa.project.validator.validators.SecQuestionEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserUnitTests {
    final User testUser = new User();

    @Test
    public void setGetUsername() {
        testUser.setUsername("test");
        assertEquals(testUser.getUsername(),"test");
    }

    @Test
    public void setGetPassword() {
        testUser.setPassword("test");
        assertEquals(testUser.getPassword(),"test");
    }

    @Test
    public void setGetEmail() {
        testUser.setEmail("test");
        assertEquals(testUser.getEmail(),"test");
    }
    @Test
    public void setGetPasswordConfirm() {
        testUser.setPasswordConfirm("test");
        assertEquals(testUser.getPasswordConfirm(),"test");
    }
    @Test
    public void setGetSecAnswer() {
        testUser.setSecAnswer("test");
        assertEquals(testUser.getSecAnswer(),"test");
    }
    @Test
    public void setGetSecQuestion() {
        testUser.setSecQuestion(SecQuestionEnum.MAIDEN_NAME);
        assertEquals(testUser.getSecQuestion(), SecQuestionEnum.MAIDEN_NAME);
    }
}
