package com.qa.project.integrationTests;

//@RunWith(SpringRunner.class)
//@SpringBootTest
//@AutoConfigureMockMvc
public class IngredientTests {

    /*@Autowired
    private MockMvc mock;

    @Autowired
    IngredientService ingredientService;
    @Autowired
    IngredientRepository ingredientRepository;
    @Autowired
    UserService userService;

    @Before
    public void init(){
        User admin = userService.findByUsername("admin123");
        admin.setOwnedIngredients(new ArrayList<Ingredient>());
        userService.save(admin);
        ingredientRepository.deleteAll();
    }

    @After
    public void destroy(){
        User admin = userService.findByUsername("admin123");
        admin.setOwnedIngredients(new ArrayList<Ingredient>());
        userService.save(admin);
        ingredientRepository.deleteAll();
    }

    @WithMockUser(value = "admin123")
    @Test
    public void ingredientSavesToDatabase() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "name", "ingredientName",
                "calories", "100",
                "carbs", "10",
                "protein", "10",
                "fats", "10",
                "username", "admin123"

        );
        String result = this.mock.perform(get("/ingredient/add/2020-01-01")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertEquals(1, ingredientService.getAll().size());
    }

    @WithMockUser(value = "admin123")
    @Test
    public void deleteIngredientFromDatabase() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "name", "ingredientName",
                "calories", "100",
                "carbs", "10",
                "protein", "10",
                "fats", "10",
                "username", "admin123"

        );
        String result = this.mock.perform(get("/ingredient/add/2020-01-01")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertEquals(1, ingredientService.getAll().size());

        data = buildUrlEncodedFormEntity(
                "id", ingredientService.getAll().get(0).getId().toString(),
                "username", "admin123"

        );
        this.mock.perform(get("/ingredient/delete/01-01-2020")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertEquals(0, ingredientService.getAll().size());
    }


    @WithMockUser(value = "admin123")
    @Test
    public void updateIngredientFromDatabase() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "name", "ingredientName",
                "calories", "100",
                "carbs", "10",
                "protein", "10",
                "fats", "10",
                "username", "admin123"

        );
        String result = this.mock.perform(get("/ingredient/add/2020-01-01")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertEquals(1, ingredientService.getAll().size());
                data = buildUrlEncodedFormEntity(
                        "id", ingredientService.getAll().get(0).getId().toString(),
                        "name", "ingredientName",
                        "calories", "1",
                        "carbs", "1",
                        "protein", "1",
                        "fats", "1"

                );
        this.mock.perform(get("/ingredient/update")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertEquals(1, ingredientService.getAll().size());
        assertEquals(1, ingredientService.getAll().get(0).getCalories().intValue());
        assertEquals(1, ingredientService.getAll().get(0).getCarbs().intValue());
        assertEquals(1, ingredientService.getAll().get(0).getProtein().intValue());
        assertEquals(1, ingredientService.getAll().get(0).getFats().intValue());
    }

    private String buildUrlEncodedFormEntity(String... params) {
        if( (params.length % 2) > 0 ) {
            throw new IllegalArgumentException("Need to give an even number of parameters");
        }
        StringBuilder result = new StringBuilder();
        for (int i=0; i<params.length; i+=2) {
            if( i > 0 ) {
                result.append('&');
            }
            try {
                result.
                        append(URLEncoder.encode(params[i], StandardCharsets.UTF_8.name())).
                        append('=').
                        append(URLEncoder.encode(params[i+1], StandardCharsets.UTF_8.name()));
            }
            catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        }
        return result.toString();
    }*/
}
