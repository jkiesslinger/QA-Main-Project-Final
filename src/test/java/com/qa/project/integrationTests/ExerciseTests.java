package com.qa.project.integrationTests;

//@RunWith(SpringRunner.class)
//@SpringBootTest
//@AutoConfigureMockMvc
public class ExerciseTests {
/*
    @Autowired
    private MockMvc mock;

    @Autowired
    ExerciseService exerciseService;
    @Autowired
    ExerciseRepository exerciseRepository;
    @Autowired
    UserService userService;
    @Before
    public void init(){
        User admin = userService.findByUsername("admin123");
        admin.setOwnedExercises(new ArrayList<Exercise>());
        userService.save(admin);
        exerciseRepository.deleteAll();
    }
    @After
    public void destroy(){
        User admin = userService.findByUsername("admin123");
        admin.setOwnedExercises(new ArrayList<Exercise>());
        userService.save(admin);
        exerciseRepository.deleteAll();
    }

    @WithMockUser(value = "admin123")
    @Test
    public void exerciseSavesToDatabase() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "name", "exerciseName",
                "caloriesBurnedPerRep", "100",
                "muscleGroupOne", "Arms",
                "muscleGroupTwo", "Legs",
                "username", "admin123"

        );
        String result = this.mock.perform(get("/exercise/add/2020-01-01")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertEquals(1, exerciseService.getAll().size());
    }

    @WithMockUser(value = "admin123")
    @Test
    public void deleteExerciseFromDatabase() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "name", "exerciseName",
                "caloriesBurnedPerRep", "100",
                "muscleGroupOne", "Arms",
                "muscleGroupTwo", "Legs",
                "username", "admin123"

        );
        String result = this.mock.perform(get("/exercise/add/2020-01-01")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertEquals(1, exerciseService.getAll().size());

        data = buildUrlEncodedFormEntity(
                "id", exerciseService.getAll().get(0).getId().toString(),
                "username", "admin123"

        );
        this.mock.perform(get("/exercise/delete")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertEquals(0, exerciseService.getAll().size());
    }


    @WithMockUser(value = "admin123")
    @Test
    public void updateExerciseFromDatabase() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "name", "exerciseName",
                "caloriesBurnedPerRep", "100",
                "muscleGroupOne", "Arms",
                "muscleGroupTwo", "Legs",
                "username", "admin123"

        );
        String result = this.mock.perform(get("/exercise/add/2020-01-01")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertEquals(1, exerciseService.getAll().size());
        data = buildUrlEncodedFormEntity(
                "id",exerciseService.getAll().get(0).getId().toString(),
                "name", "exerciseName",
                "caloriesBurnedPerRep", "1",
                "muscleGroupOne", "Chest",
                "muscleGroupTwo", "Abs"

        );
        this.mock.perform(get("/exercise/update")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertEquals(1, exerciseService.getAll().size());
        assertEquals(1, exerciseService.getAll().get(0).getCaloriesBurnedPerRep().intValue());
        assertEquals("Chest", exerciseService.getAll().get(0).getMuscleGroupOne());
        assertEquals("Abs", exerciseService.getAll().get(0).getMuscleGroupTwo());
    }

    private String buildUrlEncodedFormEntity(String... params) {
        if( (params.length % 2) > 0 ) {
            throw new IllegalArgumentException("Need to give an even number of parameters");
        }
        StringBuilder result = new StringBuilder();
        for (int i=0; i<params.length; i+=2) {
            if( i > 0 ) {
                result.append('&');
            }
            try {
                result.
                        append(URLEncoder.encode(params[i], StandardCharsets.UTF_8.name())).
                        append('=').
                        append(URLEncoder.encode(params[i+1], StandardCharsets.UTF_8.name()));
            }
            catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        }
        return result.toString();
    }*/
}
