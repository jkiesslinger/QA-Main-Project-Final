package com.qa.project.integrationTests;

//@RunWith(SpringRunner.class)
//@SpringBootTest
//@AutoConfigureMockMvc
public class PageTests {
    /*@Autowired
    private MockMvc mock;

    @WithMockUser(value = "admin123")
    @Test
    public void homePageLoads() throws Exception {
        String result = this.mock.perform(get("/home"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Summary</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void homePageLoadsWithIngredients() throws Exception {
        String result = this.mock.perform(get("/home"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Summary</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void mealTablePageLoadsWithIngredients() throws Exception {

        String data = buildUrlEncodedFormEntity(
                "name", "testName"

        );

        String result = this.mock.perform(get("/meal-table/update/20-01-2020?selected%5B0%5D=1&action=update")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Nutrition</title>"));
    }

    @WithMockUser(value = "admin123")
    @Test
    public void routineTablePageLoadsWithIngredients() throws Exception {

        String data = buildUrlEncodedFormEntity(
                "name", "testName"

        );

        String result = this.mock.perform(get("/routine-table/update/20-01-2020?selected%5B0%5D=1&action=update")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Training</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void routineTableSavePageLoadsWithIngredients() throws Exception {

        String data = buildUrlEncodedFormEntity(
                "name", "testName"

        );

        String result = this.mock.perform(get("/routine-table/update/20-01-2020?selected%5B0%5D=-1&action=Save")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Training</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void homePageLoadsWithForwardSlash() throws Exception {
        String result = this.mock.perform(get("/"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Nutrition & Exercise Tracker</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void ingredientTablePageLoads() throws Exception {
        String result = this.mock.perform(get("/ingredient-table"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Nutrition</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void calorieCounterLoadsWithData() throws Exception {

        String result = this.mock.perform(get("/calorie-counter"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Nutrition</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void exerciseCounterLoadsWithData() throws Exception {

        String result = this.mock.perform(get("/exercise-counter"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Nutrition & Exercise Tracker</title>"));
    }

    @WithMockUser(value = "admin123")
    @Test
    public void exerciseTablePageLoads() throws Exception {
        String result = this.mock.perform(get("/exercise-table"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Training</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void changeIngredientPageLoads() throws Exception {
        String result = this.mock.perform(get("/changeIngredient"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Nutrition</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void changeExercisePageLoads() throws Exception {
        String result = this.mock.perform(get("/changeExercise"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Training</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void deleteExercisePageLoads() throws Exception {
        String result = this.mock.perform(get("/delete-exercise"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Training</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void deleteIngredientPageLoads() throws Exception {
        String result = this.mock.perform(get("/delete-ingredient"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Nutrition</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void mealTablePageLoads() throws Exception {
        String result = this.mock.perform(get("/meal-table"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Nutrition</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void routineTablePageLoads() throws Exception {
        String result = this.mock.perform(get("/routine-table"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Training</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void signupPageLoads() throws Exception {
        String result = this.mock.perform(get("/signup"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Create an account</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void accDetailsPageLoads() throws Exception {
        String result = this.mock.perform(get("/accDetails"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Settings</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void settingsPageLoads() throws Exception {
        String result = this.mock.perform(get("/settings/1"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Settings</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void passRecoverPageLoads() throws Exception {
        String result = this.mock.perform(get("/passRecover"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Recover Password</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void nutritionPageLoads() throws Exception {
        String result = this.mock.perform(get("/nutrition"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Nutrition</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void nutritionUpdateDatePageLoads() throws Exception {
        String result = this.mock.perform(get("/nutrition/dateUpdate"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Nutrition</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void trainingPageLoads() throws Exception {
        String result = this.mock.perform(get("/training"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Training</title>"));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void TrainingUpdateDatePageLoads() throws Exception {
        String result = this.mock.perform(get("/training/dateUpdate"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Training</title>"));
    }
    @Test
    public void loginPageLoads() throws Exception {
        String result = this.mock.perform(get("/login"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Login</title>"));
    }

    @WithMockUser(value = "admin123")
    @Test
    public void anythingElseReturnsIndex() throws Exception {
        String result = this.mock.perform(get("/anythingElse"))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Nutrition & Exercise Tracker</title>"));
    }
    private String buildUrlEncodedFormEntity(String... params) {
        if( (params.length % 2) > 0 ) {
            throw new IllegalArgumentException("Need to give an even number of parameters");
        }
        StringBuilder result = new StringBuilder();
        for (int i=0; i<params.length; i+=2) {
            if( i > 0 ) {
                result.append('&');
            }
            try {
                result.
                        append(URLEncoder.encode(params[i], StandardCharsets.UTF_8.name())).
                        append('=').
                        append(URLEncoder.encode(params[i+1], StandardCharsets.UTF_8.name()));
            }
            catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        }
        return result.toString();
    }*/
}
