package com.qa.project.integrationTests;

import com.qa.project.persistence.model.*;
import com.qa.project.persistence.repo.MealHistoryRepository;
import com.qa.project.persistence.repo.RoleRepository;
import com.qa.project.persistence.repo.RoutineHistoryRepository;
import com.qa.project.persistence.repo.UserRepository;
import com.qa.project.service.UserService;
import com.qa.project.validator.validators.SecQuestionEnum;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserTests {

    @Autowired
    private MockMvc mock;

    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    private String email;
    private String password;
    private String passwordConfirm;
    private String secAnswer;
    private String username;
    @Autowired
    private MealHistoryRepository mealHistoryRepository;
    @Autowired
    private RoutineHistoryRepository routineHistoryRepository;

    @Before
    public void init() throws Exception {
        List<MealHistory> allMealHistories = mealHistoryRepository.findAll();
        for(MealHistory m : allMealHistories){
            m.setOwnedBy(null);
            mealHistoryRepository.save(m);
        }
        List<RoutineHistory> allRoutineHistories = routineHistoryRepository.findAll();
        for(RoutineHistory r : allRoutineHistories){
            r.setOwnedBy(null);
            routineHistoryRepository.save(r);
        }
        List<User> allUsers = userRepository.findAll();
        for(User u : allUsers){
            u.setOwnedIngredients(new ArrayList<Ingredient>());
            u.setOwnedExercises(new ArrayList<Exercise>());
            userRepository.save(u);
        }

        userRepository.deleteAll();
        this.email = "james@james.com";
        this.password ="Abc123456**";
        this.passwordConfirm="Abc123456**";
        this.secAnswer="TestAnswer";
        String secQuestion = "What is your mother's maiden name?";
        this.username="admin123";

        String data = buildUrlEncodedFormEntity(
                "email", this.email,
                "username", this.username,
                "password", this.password,
                "passwordConfirm", this.passwordConfirm,
                "secQuestionString", secQuestion,
                "secAnswer", this.secAnswer
        );
        String result = this.mock.perform(post("/adduser")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertEquals(1, userService.getAll().size());
    }

    @After
    public void destroy(){
        List<MealHistory> allMealHistories = mealHistoryRepository.findAll();
        for(MealHistory m : allMealHistories){
            m.setOwnedBy(null);
            mealHistoryRepository.save(m);
        }
        List<RoutineHistory> allRoutineHistories = routineHistoryRepository.findAll();
        for(RoutineHistory r : allRoutineHistories){
            r.setOwnedBy(null);
            routineHistoryRepository.save(r);
        }
        List<User> allUsers = userRepository.findAll();
        for(User u : allUsers){
            u.setOwnedIngredients(new ArrayList<Ingredient>());
            u.setOwnedExercises(new ArrayList<Exercise>());
            userRepository.save(u);
        }
        userRepository.deleteAll();
        User admin = new User("admin123", "Abc123456**", "Abc123456**", "james@gmail.com", "Abc123456", SecQuestionEnum.MAIDEN_NAME);
        admin.setRoles(roleRepository.findAll());

        User user = new User("user123", "Abc123456**", "Abc123456**", "james2@gmail.com", "Abc123456", SecQuestionEnum.MAIDEN_NAME);
        user.setRoles(roleRepository.findRoleByName("ROLE_USER"));

        userService.save(admin);
        userService.save(user);
    }


    @Test
    public void createUser() {
        assertEquals(1, userService.getAll().size());
    }

    @Test
    public void recoverAccount() throws Exception {
        assertEquals(1, userService.getAll().size());
        String data = buildUrlEncodedFormEntity(
                "email", this.email
        );
        String result = this.mock.perform(post("/recover")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("What is your mother&#39;s maiden name?"));
        data = buildUrlEncodedFormEntity(
                "email", this.email,
                "secAnswer", this.secAnswer
        );
        result = this.mock.perform(post("/recover")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("Success! New password: "));
    }
    @WithMockUser(value = "admin123")
    @Test
    public void changeSettings() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "targetCalorieBurned", "999",
                "targetCalorieIntake", "999",
                "targetCarbIntake", "999",
                "targetProteinIntake", "999",
                "targetFatIntake", "999"

        );

        this.mock.perform(get("/changeSettings")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertEquals(1, userService.getAll().size());
        User user = userService.getAll().get(0);
        assertEquals(999, user.getTargetCalorieBurned().intValue());
        assertEquals(999, user.getTargetCalorieIntake().intValue());
        assertEquals(999, user.getTargetCarbIntake().intValue());
        assertEquals(999, user.getTargetFatIntake().intValue());
        assertEquals(999, user.getTargetProteinIntake().intValue());
    }
    @WithMockUser(value = "admin123")
    @Test
    public void updateUser() throws Exception {

        String data = buildUrlEncodedFormEntity(
                        "id", userService.getAll().get(0).getId().toString(),
                        "email", this.email,
                        "username", this.username + "1",
                        "password", this.password + "1",
                        "passwordConfirm", this.passwordConfirm + "1",
                        "secAnswer", this.secAnswer + "1"

                );

        this.mock.perform(post("/changeAccountSettings")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertEquals(1, userService.getAll().size());
        User testUser = userService.findByUsername("admin1231");
        assertEquals(this.email, testUser.getEmail());
        assertEquals(this.secAnswer + "1", testUser.getSecAnswer());
        assertEquals(this.username + "1", testUser.getUsername());
    }


    private String buildUrlEncodedFormEntity(String... params) {
        if( (params.length % 2) > 0 ) {
            throw new IllegalArgumentException("Need to give an even number of parameters");
        }
        StringBuilder result = new StringBuilder();
        for (int i=0; i<params.length; i+=2) {
            if( i > 0 ) {
                result.append('&');
            }
            try {
                result.
                        append(URLEncoder.encode(params[i], StandardCharsets.UTF_8.name())).
                        append('=').
                        append(URLEncoder.encode(params[i+1], StandardCharsets.UTF_8.name()));
            }
            catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        }
        return result.toString();
    }
}
