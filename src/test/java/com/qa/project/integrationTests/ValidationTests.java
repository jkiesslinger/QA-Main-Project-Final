package com.qa.project.integrationTests;


import com.qa.project.persistence.model.*;
import com.qa.project.persistence.repo.MealHistoryRepository;
import com.qa.project.persistence.repo.RoutineHistoryRepository;
import com.qa.project.persistence.repo.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ValidationTests {
    int i = 4;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MealHistoryRepository mealHistoryRepository;

    @Autowired
    private RoutineHistoryRepository routineHistoryRepository;

    @Autowired
    private MockMvc mock;

    private String email;
    private String password;
    private String passwordConfirm;
    private String secAnswer;
    private String secQuestion;
    private String username;

    @Before
    public void init(){
        List<MealHistory> allMealHistories = mealHistoryRepository.findAll();
        for(MealHistory m : allMealHistories){
            m.setOwnedBy(null);
            mealHistoryRepository.save(m);
        }
        List<RoutineHistory> allRoutineHistories = routineHistoryRepository.findAll();
        for(RoutineHistory r : allRoutineHistories){
            r.setOwnedBy(null);
            routineHistoryRepository.save(r);
        }
        List<User> allUsers = userRepository.findAll();
        for(User u : allUsers){
            u.setOwnedIngredients(new ArrayList<Ingredient>());
            u.setOwnedExercises(new ArrayList<Exercise>());
            userRepository.save(u);
        }

        userRepository.deleteAll();
        this.email = "james@james.com";
        this.password ="Abc123456**";
        this.passwordConfirm="Abc123456**";
        this.secAnswer="TestAnswer";
        this.secQuestion="What is your mother's maiden name?";
        this.username="james12345";

    }
    @Test
    public void validCustomerRedirectsToLogin() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "email", this.email,
                "password", this.password,
                "passwordConfirm", this.passwordConfirm,
                "secAnswer", this.secAnswer,
                "secQuestionString", this.secQuestion,
                "username", this.username

        );
        String result = this.mock.perform(post("/adduser")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Login</title>"));
    }

    @Test
    public void invalidEmailDoesNotRedirect() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "email", "abc",
                "password", this.password,
                "passwordConfirm", this.passwordConfirm,
                "secAnswer", this.secAnswer,
                "secQuestion", this.secQuestion,
                "username", this.username

        );
        String result = this.mock.perform(post("/adduser")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Create an account</title>"));
    }

    @Test
    public void invalidPasswordTooShortDoesNotRedirect() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "email", this.email,
                "password", "Abc123*",
                "passwordConfirm", "Abc123*",
                "secAnswer", this.secAnswer,
                "secQuestion", this.secQuestion,
                "username", this.username

        );
        String result = this.mock.perform(post("/adduser")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Create an account</title>"));
    }
    @Test
    public void invalidPasswordTooLongDoesNotRedirect() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "email", this.email,
                "password", "Abc123*Abc123*Abc123*Abc123*Abc123*Abc1",
                "passwordConfirm", "Abc123*Abc123*Abc123*Abc123*Abc123*Abc1",
                "secAnswer", this.secAnswer,
                "secQuestion", this.secQuestion,
                "username", this.username

        );
        String result = this.mock.perform(post("/adduser")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Create an account</title>"));
    }
    @Test
    public void invalidPasswordNoNumberDoesNotRedirect() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "email", this.email,
                "password", "AbcAbc**",
                "passwordConfirm", "AbcAbc**",
                "secAnswer", this.secAnswer,
                "secQuestion", this.secQuestion,
                "username", this.username

        );
        String result = this.mock.perform(post("/adduser")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Create an account</title>"));
    }
    @Test
    public void invalidPasswordNoUppercaseDoesNotRedirect() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "email", this.email,
                "password", "abcabc1**",
                "passwordConfirm", "abcabc1**",
                "secAnswer", this.secAnswer,
                "secQuestion", this.secQuestion,
                "username", this.username

        );
        String result = this.mock.perform(post("/adduser")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Create an account</title>"));
    }
    @Test
    public void invalidPasswordNoLowercaseDoesNotRedirect() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "email", this.email,
                "password", "ABCABC1**",
                "passwordConfirm", "ABCABC1**",
                "secAnswer", this.secAnswer,
                "secQuestion", this.secQuestion,
                "username", this.username

        );
        String result = this.mock.perform(post("/adduser")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Create an account</title>"));
    }

    @Test
    public void invalidPasswordNoSpecialCharDoesNotRedirect() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "email", this.email,
                "password", "ABCABC1a",
                "passwordConfirm", "ABCABC1a",
                "secAnswer", this.secAnswer,
                "secQuestion", this.secQuestion,
                "username", this.username

        );
        String result = this.mock.perform(post("/adduser")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Create an account</title>"));
    }

    @Test
    public void invalidPasswordsDoNotMatchDoesNotRedirect() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "email", this.email,
                "password", "Abc123456**",
                "passwordConfirm", "Abc123456**1",
                "secAnswer", this.secAnswer,
                "secQuestion", this.secQuestion,
                "username", this.username

        );
        String result = this.mock.perform(post("/adduser")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Create an account</title>"));
    }
    @Test
    public void invalidSecAnswerDoesNotRedirect() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "email", this.email,
                "password", this.password,
                "passwordConfirm", this.passwordConfirm,
                "secAnswer", "a",
                "secQuestion", this.secQuestion,
                "username", this.username

        );
        String result = this.mock.perform(post("/adduser")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Create an account</title>"));
    }
    @Test
    public void invalidSecQuestionDoesNotRedirect() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "email", this.email,
                "password", this.password,
                "passwordConfirm", this.passwordConfirm,
                "secAnswer", this.secAnswer,
                "secQuestion", "not a valid question",
                "username", this.username

        );
        String result = this.mock.perform(post("/adduser")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Create an account</title>"));
    }
    @Test
    public void invalidUsernameTooLongDoesNotRedirect() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "email", this.email,
                "password", this.password,
                "passwordConfirm", this.passwordConfirm,
                "secAnswer", this.secAnswer,
                "secQuestion", this.secQuestion,
                "username", "adminadminadminadminadminadminaaa"

        );
        String result = this.mock.perform(post("/adduser")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Create an account</title>"));
    }
    @Test
    public void invalidUsernameTooShortDoesNotRedirect() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "email", this.email,
                "password", this.password,
                "passwordConfirm", this.passwordConfirm,
                "secAnswer", this.secAnswer,
                "secQuestion", this.secQuestion,
                "username", "admin"

        );
        String result = this.mock.perform(post("/adduser")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Create an account</title>"));
    }
    @Test
    public void invalidUsernameContainsSpecialCharsDoesNotRedirect() throws Exception {
        String data = buildUrlEncodedFormEntity(
                "email", this.email,
                "password", this.password,
                "passwordConfirm", this.passwordConfirm,
                "secAnswer", this.secAnswer,
                "secQuestion", this.secQuestion,
                "username", "admin**"

        );
        String result = this.mock.perform(post("/adduser")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(data))
                .andReturn().getResponse().getContentAsString();
        assertThat(result, containsString("<title>Create an account</title>"));
    }
    private String buildUrlEncodedFormEntity(String... params) {
        if( (params.length % 2) > 0 ) {
            throw new IllegalArgumentException("Need to give an even number of parameters");
        }
        StringBuilder result = new StringBuilder();
        for (int i=0; i<params.length; i+=2) {
            if( i > 0 ) {
                result.append('&');
            }
            try {
                result.
                        append(URLEncoder.encode(params[i], StandardCharsets.UTF_8.name())).
                        append('=').
                        append(URLEncoder.encode(params[i+1], StandardCharsets.UTF_8.name()));
            }
            catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        }
        return result.toString();
    }
}
