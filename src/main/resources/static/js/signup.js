window.onload = function getCardSize(){
    let errCount = document.getElementById("numberOfErrors").attributes.value.nodeValue;
    let cardSize=50;
    for(let i = 0; i<errCount; i++){
        cardSize += 3.57;
    }

    document.getElementById('cardToResize').style.height = (cardSize + "%");
};