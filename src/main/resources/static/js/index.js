let calorieHistory = document.getElementById("calorieData").innerText;
let calorieDates = document.getElementById("calorieDates").innerText;
let muscleGroups = document.getElementById("muscleGroups").innerText;
let muscleGroupFocus = document.getElementById("muscleGroupFocus").innerText;

let pieData = muscleGroupFocus.split(',');
let pieLabels = muscleGroups.split(',');

if(pieData.length===1 && pieData[0]===""){
  pieData = [100];
  pieLabels = ["No Data"];
}
const regex = RegExp(',');
let data = 0;
if(regex.test(calorieHistory)){
  data = calorieHistory.split(',');
}else if(calorieHistory.length>0){
  data[0] = calorieHistory;
}

let dataDates = "";
if(regex.test(calorieDates)){
  dataDates = calorieDates.split(',');
}else if(calorieDates.length>0){
  data[0] = calorieDates;
}

let lastFive = [0,0,0,0,0];
if(data.length > 4){
  lastFive = data.slice(0,5);
  lastFive=lastFive.reverse();
}
let lastTen = [0,0,0,0,0,0,0,0,0,0];
if(data.length > 9){
  lastTen = data.slice(0,10);
  lastTen=lastTen.reverse();
}
let lastTwenty = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
if(data.length > 19){
  lastTwenty = data.slice(0,20);
  lastTwenty=lastTwenty.reverse();
}
let lastFiveDates = [0,0,0,0,0];
if(dataDates.length > 4){
  lastFiveDates = dataDates.slice(0,5);
  lastFiveDates=lastFiveDates.reverse();
}
let lastTenDates = [0,0,0,0,0,0,0,0,0,0];
if(dataDates.length > 9){
  lastTenDates = dataDates.slice(0,10);
  lastTenDates=lastTenDates.reverse();
}
let lastTwentyDates = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
if(dataDates.length > 19){
  lastTwentyDates = dataDates.slice(0,20);
  lastTwentyDates=lastTwentyDates.reverse();
}


let x = lastFiveDates;
let y = lastFive;
let lineTitle = "Progress over one week";


let line = new Chart(document.getElementById("lineChart"), {
  type: 'line',
  data: {
    labels: x,
    datasets: [
      {
        data: y,
        label: "Kcal",
        borderColor: "#3e95cd",
        fill: true
      }
    ]
  },
  options: {

    title: {
      display: true,
      text: 'Calorie intake last 5 entries'
    }
  }

});

const pie = new Chart(document.getElementById("pieChart"), {
  type: 'pie',
  data: {
    labels: pieLabels,
    datasets: [{
      label: "Muscle Group Focus",
      backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f"],
      data: pieData
    }]
  },
  options: {
    title: {
      display: true,
      text: '% of time spent on each group'
    }
  }
});


const lineChoice = document.getElementById("lineChartRange");
lineChoice.addEventListener('change', function(){
  if(lineChoice.value == "20"){
    x=lastTwentyDates;
    y=lastTwenty;
    lineTitle = "Progress over six months"
  }else if(lineChoice.value == "10"){
    x=lastTenDates;
    y=lastTen;
    lineTitle = "Progress over one month"
  }else if(lineChoice.value == "5"){
    x=lastFiveDates;
    y=lastFive;
    lineTitle = "Progress over one week"
  }
  document.getElementById("lineCanvasContainer").innerHTML = '&nbsp;';
  document.getElementById("lineCanvasContainer").innerHTML = '<canvas id="lineChart" style="height:auto; width:auto"></canvas>';
  line = new Chart(document.getElementById("lineChart"), {
      type: 'line',
      data: {
        labels: x,
        datasets: [
          { 
        data: y,
        label: "Kcal",
        borderColor: "#3e95cd",
        fill: true
      }
        ]
      },
        options: {
          
          title: {
            display: true,
            text: lineTitle
          }
        }
        
    });
    line.render();
}, false);