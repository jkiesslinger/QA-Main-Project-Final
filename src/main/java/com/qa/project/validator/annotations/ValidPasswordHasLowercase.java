package com.qa.project.validator.annotations;

import com.qa.project.validator.validators.PasswordLowercaseValidator;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({TYPE, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = PasswordLowercaseValidator.class)
@Documented
public @interface ValidPasswordHasLowercase {
    String message() default "Password must contain lowercase chars";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}