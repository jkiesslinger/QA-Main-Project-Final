package com.qa.project.validator.annotations;

import com.qa.project.validator.validators.UsernameLengthValidator;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({TYPE, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = UsernameLengthValidator.class)
@Documented
public @interface ValidUsername6To32Char {
    String message() default "Username must be between 6 and 32 char";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
