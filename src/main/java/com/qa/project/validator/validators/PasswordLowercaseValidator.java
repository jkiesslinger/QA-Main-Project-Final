package com.qa.project.validator.validators;

import com.qa.project.validator.annotations.ValidPasswordHasLowercase;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordLowercaseValidator implements ConstraintValidator<ValidPasswordHasLowercase, String> {
    private static final String PASSWORD_PATTERN = "(.*[a-z].*)";
    @Override
    public void initialize(ValidPasswordHasLowercase constraintAnnotation) {
    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context){
        return (validatePassword(password));
    }
    private boolean validatePassword(String password) {
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }
}
