package com.qa.project.validator.validators;

import com.qa.project.validator.annotations.ValidUsernameHasNoSpecialChar;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UsernameSpecialCharValidator implements ConstraintValidator<ValidUsernameHasNoSpecialChar, String> {
    private static final String USERNAME_PATTERN = "(.*[!@#$%^&*(),.?\":{}|<>].*)";
    @Override
    public void initialize(ValidUsernameHasNoSpecialChar constraintAnnotation) {
    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context){
        return (validateEmail(email));
    }
    private boolean validateEmail(String email) {
        Pattern pattern = Pattern.compile(USERNAME_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return !matcher.matches();
    }
}
