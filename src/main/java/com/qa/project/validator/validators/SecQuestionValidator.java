package com.qa.project.validator.validators;

import com.qa.project.validator.annotations.ValidSecQuestion;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SecQuestionValidator implements ConstraintValidator<ValidSecQuestion, String> {

    @Override
    public void initialize(ValidSecQuestion constraintAnnotation) {
    }

    @Override
    public boolean isValid(String secQ, ConstraintValidatorContext context){
        return (validateSecQ(secQ));
    }
    private boolean validateSecQ(String secQ) {
        SecQuestionHashmap mappings = new SecQuestionHashmap();
        for(String i : mappings.getMap().keySet()){
            if(i.equals(secQ)){
                return true;
            }
        }
        return false;
    }
}
