package com.qa.project.validator.validators;

public enum SecQuestionEnum {
    MAIDEN_NAME,
    FIRST_PET_NAME,
    FAVE_SPORT_TEAM
}
