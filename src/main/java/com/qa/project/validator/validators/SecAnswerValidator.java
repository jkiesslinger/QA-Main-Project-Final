package com.qa.project.validator.validators;

import com.qa.project.validator.annotations.ValidSecAnswer;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SecAnswerValidator implements ConstraintValidator<ValidSecAnswer, String> {

    @Override
    public void initialize(ValidSecAnswer constraintAnnotation) {
    }

    @Override
    public boolean isValid(String username, ConstraintValidatorContext context){
        return (validateUsername(username));
    }
    private boolean validateUsername(String username) {
        if(username.length()<6){
            return false;
        }else return username.length() <= 32;
    }
}
