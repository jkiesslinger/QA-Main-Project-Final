package com.qa.project.validator.validators;

import com.qa.project.validator.annotations.ValidUsername6To32Char;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UsernameLengthValidator implements ConstraintValidator<ValidUsername6To32Char, String> {

    @Override
    public void initialize(ValidUsername6To32Char constraintAnnotation) {
    }

    @Override
    public boolean isValid(String username, ConstraintValidatorContext context){
        return (validateUsername(username));
    }
    private boolean validateUsername(String username) {
        if(username.length()<6){
            return false;
        }else return username.length() <= 32;
    }
}
