package com.qa.project.validator.validators;

import java.util.HashMap;
import com.qa.project.validator.validators.SecQuestionEnum;

public class SecQuestionHashmap {
    final HashMap<String,SecQuestionEnum> mappings = new HashMap<String,SecQuestionEnum>();

    public SecQuestionHashmap() {
        this.mappings.put("What is your mother's maiden name?",SecQuestionEnum.MAIDEN_NAME);
        this.mappings.put("What was your first pet's name?",SecQuestionEnum.FIRST_PET_NAME);
        this.mappings.put("What is your favourite sports team?",SecQuestionEnum.FAVE_SPORT_TEAM);
    }
    public HashMap<String,SecQuestionEnum> getMap(){
        return this.mappings;
    }
}
