package com.qa.project.validator.validators;

import com.qa.project.validator.annotations.ValidPasswordHasSpecialChar;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordSpecialCharValidator implements ConstraintValidator<ValidPasswordHasSpecialChar, String> {
    private static final String EMAIL_PATTERN = "(.*[!@#$%^&*(),.?\":{}|<>].*)";
    @Override
    public void initialize(ValidPasswordHasSpecialChar constraintAnnotation) {
    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context){
        return (validatePassword(password));
    }
    private boolean validatePassword(String password) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }
}
