package com.qa.project.validator.validators;

import com.qa.project.validator.annotations.ValidPassword8to32Char;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordLengthValidator implements ConstraintValidator<ValidPassword8to32Char, String> {

    @Override
    public void initialize(ValidPassword8to32Char constraintAnnotation) {
    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context){
        return (validatePassword(password));
    }
    private boolean validatePassword(String password) {
        if(password.length()<8){
            return false;
        }else return password.length() <= 32;
    }
}
