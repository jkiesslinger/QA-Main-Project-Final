package com.qa.project.controllers;

import com.qa.project.persistence.model.Ingredient;
import com.qa.project.persistence.model.MealHistory;
import com.qa.project.persistence.model.TempMeal;
import com.qa.project.persistence.model.User;
import com.qa.project.service.MealHistoryService;
import com.qa.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
public class MealHistoryController {
    @Autowired
    private MealHistoryService mealHistoryService;
    @Autowired
    private UserService userService;
    @GetMapping({"/calorie-counter", "/calorie-counter/{date}"})
    public String calorieCounter(@PathVariable(value="date", required=false) String d, Model model) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        if(d == null){
            Date date = Calendar.getInstance().getTime();
            d = dateFormat.format(date);

        }
        model.addAttribute("date", d);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        List<Integer> targets = new ArrayList<Integer>();
        User activeUser = userService.findByUsername(username);
        targets.add(activeUser.getTargetCalorieIntake());
        targets.add(activeUser.getTargetProteinIntake());
        targets.add(activeUser.getTargetCarbIntake());
        targets.add(activeUser.getTargetFatIntake());
        model.addAttribute("targets",targets);
        List<MealHistory> userMealHistory = activeUser.getMealHistories();
        Boolean found = false;
        MealHistory foundMealHistory = new MealHistory();
        Date date2 = formatter.parse(d);
        String d2 = dateFormat.format(date2);
        for(MealHistory m : userMealHistory){
            if (dateFormat.format(m.getDate()).equals(d2)){
                found = true;
                foundMealHistory = m;
            }
        }
        List<Integer> progressToTargets = new ArrayList<Integer>();
        int totalProtein = 0;
        int totalCarb = 0;
        int totalFat = 0;
        int totalCalorieIn = 0;
        if(found){
            for(Ingredient i : foundMealHistory.getMealHistoryIngredient()){
                totalCalorieIn += i.getCalories();
                totalFat+= i.getFats();
                totalCarb+= i.getCarbs();
                totalProtein += i.getProtein();
            }
        }

        progressToTargets.add(totalCalorieIn);
        progressToTargets.add(totalProtein);
        progressToTargets.add(totalCarb);
        progressToTargets.add(totalFat);

        model.addAttribute("progress", progressToTargets);
        return "calorie-counter";
    }

    @GetMapping("/meal-table/update/{date}")
    public String mealTableUpdate(@PathVariable(value="date") String d, @ModelAttribute TempMeal form, Model model, @RequestParam(value="action") String action) throws ParseException {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        User activeUser = userService.findByUsername(username);
        form.removeNulls();
        if(action.equals("Save")){
            List<MealHistory> currentMealHistories = activeUser.getMealHistories();
            MealHistory mealHistory = new MealHistory();
            mealHistory.setOwnedBy(activeUser);
            mealHistory.setMealHistoryIngredient(form.getSelected());
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date date= formatter.parse(d);
            mealHistory.setDate(date);
            Boolean found = false;
            for(MealHistory m : currentMealHistories){
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                if (dateFormat.format(m.getDate()).equals(d)){
                    found = true;
                    mealHistory.setId(m.getId());
                }
            }
            mealHistoryService.save(mealHistory);
            if(!found){
                currentMealHistories.add(mealHistory);
                activeUser.setMealHistories(currentMealHistories);
                userService.save(activeUser);
            }
        }
        List<Ingredient> foundIngredients = activeUser.getOwnedIngredients();
        for(Ingredient i : foundIngredients){
            form.addIngredient(i);
        }
        if(form.getSelected().size() <10){
            form.addSelected(new Ingredient());
        }

        model.addAttribute("form", form);
        model.addAttribute("date", d);

        return "meal-table";
    }
}
