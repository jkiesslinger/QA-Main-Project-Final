package com.qa.project.controllers;

import com.qa.project.persistence.model.*;
import com.qa.project.service.ExerciseService;
import com.qa.project.service.RoutineHistoryService;
import com.qa.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.qa.project.helper.Constants.*;

@Controller
public class ExerciseController {
    @Autowired
    private ExerciseService exerciseService;

    @Autowired
    private UserService userService;

    @Autowired
    RoutineHistoryService routineHistoryService;
    SimpleDateFormat formatter = new SimpleDateFormat(yearMonthDay);

    DateFormat dateFormat = new SimpleDateFormat(dayMonthYear);

    @GetMapping({exerciseCreateMapping, exerciseCreateMappingWithDate})
    public String addExercise(@PathVariable(value=pageDate, required=false) String date, @ModelAttribute("exerciseForm") Exercise exerciseForm) throws ParseException {
        date = formatDateYearFirstToDayFirst(date);
        saveExerciseToUser(exerciseForm, getUser());
        if(date.equals(nullString)){
            return exerciseRedirect;
        }
        return exerciseRedirect + date;
    }

    @GetMapping({exerciseUpdateMapping, exerciseUpdateMappingWithDate})
    public String updateExercise(@PathVariable(value=pageDate, required=false) String date, @ModelAttribute("exerciseForm") Exercise exerciseForm) {
        updateExerciseToUser(getUser(), exerciseForm);
        return exerciseRedirect + date;
    }

    @GetMapping({exerciseDeleteMapping, exerciseDeleteMappingWithDate})
    public String deleteExercise(@PathVariable(value=pageDate, required=false) String date, @ModelAttribute("exerciseForm") Exercise exerciseForm) {
        //try catch
        if(exerciseForm.getId()==null){
            return exerciseRedirect + date;
        }
        User user = getUser();
        Exercise toRemove = exerciseService.getOneById(exerciseForm.getId());
        if(isOwnedByLoggedInUser(toRemove,user)){
            purgeExercise(toRemove, user);
        }
        return exerciseRedirect + date;
    }

    private User getUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            String username = ((UserDetails)principal).getUsername();
            return userService.findByUsername(username);
        } else {
            String username = principal.toString();
            return userService.findByUsername(username);
        }
    }

    private String formatDateYearFirstToDayFirst(String date) throws ParseException {
        Date datePlaceholder = formatter.parse(date);
        return dateFormat.format(datePlaceholder);
    }

    private void saveExerciseToUser(Exercise exerciseForm, User user){
        exerciseForm.setOwnedBy(user);
        Exercise savedExercise = exerciseService.save(exerciseForm);
        List<Exercise> exercises = user.getOwnedExercises();
        exercises.add(savedExercise);
        user.setOwnedExercises(exercises);
        //try catch
        if(savedExercise.getId() != null){
            userService.save(user);
        }
    }

    private void updateExerciseToUser(User user, Exercise exerciseForm){
        exerciseForm.setOwnedBy(user);
        for(Exercise exercise : user.getOwnedExercises()){
            if(exercise.getId().equals(exerciseForm.getId())){
                exerciseForm.setName(exercise.getName());
                exerciseService.save(exerciseForm);
            }
        }
    }

    private Boolean isOwnedByLoggedInUser(Exercise toRemove, User user){
        return toRemove.getOwnedBy().equals(user);
    }

    private void purgeExercise(Exercise toRemove, User user){
        purgeExerciseFromUser(toRemove, user);
        purgeExerciseFromHistories(toRemove, user);
        exerciseService.delete(toRemove.getId());
    }

    private void purgeExerciseFromUser(Exercise toRemove, User user){
        user.getOwnedExercises().remove(toRemove);
        userService.save(user);
    }

    private List<RoutineHistory> getAllHistoriesContainingExercise(Exercise toRemove, User user){
        List<RoutineHistory> exerciseLocatedInRoutineHistories = new ArrayList<>();
        for(RoutineHistory routineHistory : user.getRoutineHistories()){
            for(Exercise exercise : routineHistory.getRoutineHistoryExercise()){
                if(exercise.getId().equals(toRemove.getId())){
                    exerciseLocatedInRoutineHistories.add(routineHistory);
                }
            }
        }
        return exerciseLocatedInRoutineHistories;
    }
    private void purgeExerciseFromHistories(Exercise toRemove, User user){
        List<RoutineHistory> exerciseLocatedInRoutineHistories = getAllHistoriesContainingExercise(toRemove, user);
        for(RoutineHistory routineHistory : exerciseLocatedInRoutineHistories){
            routineHistory.getRoutineHistoryExercise().remove(toRemove);
            routineHistoryService.save(routineHistory);
        }
    }
}
