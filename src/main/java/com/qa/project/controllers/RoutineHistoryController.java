package com.qa.project.controllers;

import com.qa.project.persistence.model.*;
import com.qa.project.service.RoutineHistoryService;
import com.qa.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
public class RoutineHistoryController {
    @Autowired
    private com.qa.project.service.RoutineHistoryService RoutineHistoryService;


    @PostMapping({"/routine_history/add"})
    public String addRoutineHistory(@ModelAttribute("routineHistoryForm") RoutineHistory routineHistoryForm) {
    	RoutineHistoryService.save(routineHistoryForm);
        return "sucess";
    }
    
    @GetMapping({"/exercise/getAll"})
    public Iterable<RoutineHistory> getAllRoutineHistories() {
    	return RoutineHistoryService.getAll();
    }

    @Autowired
    private UserService userService;
    @Autowired
    private RoutineHistoryService routineHistoryService;
    private User getUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        return userService.findByUsername(username);
    }
    private void saveAll(User activeUser, TempRoutine form, String d) throws ParseException {
        List<RoutineHistory> currentRoutineHistories = activeUser.getRoutineHistories();
        RoutineHistory routineHistory = new RoutineHistory();
        routineHistory.setOwnedBy(activeUser);
        routineHistory.setRoutineHistoryExercise(form.getSelected());
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date= formatter.parse(d);
        routineHistory.setDate(date);
        Boolean found = false;
        for(RoutineHistory r : currentRoutineHistories){
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            if (dateFormat.format(r.getDate()).equals(d)){
                found = true;
                routineHistory.setId(r.getId());
            }
        }
        routineHistoryService.save(routineHistory);
        if(!found){
            currentRoutineHistories.add(routineHistory);
            activeUser.setRoutineHistories(currentRoutineHistories);
            userService.save(activeUser);
        }
    }
    @GetMapping("/routine-table/update/{date}")
    public String routineTableUpdate(@PathVariable(value="date") String d, @ModelAttribute TempRoutine form, Model model, @RequestParam(value="action") String action) throws ParseException {
        User activeUser = getUser();
        form.removeNulls();
        if(action.equals("Save")){
            saveAll(activeUser, form, d);
        }
        List<Exercise> foundExercises = activeUser.getOwnedExercises();
        for(Exercise e : foundExercises){
            form.addExercise(e);
        }
        if(form.getSelected().size() <10){
            form.addSelected(new Exercise());
        }

        model.addAttribute("form", form);
        model.addAttribute("date", d);

        return "routine-table";
    }

    @GetMapping({"/exrecise-counter", "/exercise-counter/{date}"})
    public String calorieCounter(@PathVariable(value="date", required=false) String d, Model model) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        if(d == null){
            Date date = Calendar.getInstance().getTime();
            d = dateFormat.format(date);

        }
        model.addAttribute("date", d);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        User activeUser = getUser();
        List<RoutineHistory> userRoutineHistory = activeUser.getRoutineHistories();
        Boolean foundRoutine = false;
        RoutineHistory foundRoutineHistory = new RoutineHistory();
        for(RoutineHistory r : userRoutineHistory){
            if (dateFormat.format(r.getDate()).equals(d) || formatter.format(r.getDate()).equals(d)){
                foundRoutine = true;
                foundRoutineHistory = r;

            }
        }
        int caloriesBurned = 0;
        if(foundRoutine){
            for(Exercise e : foundRoutineHistory.getRoutineHistoryExercise()){
                caloriesBurned += e.getCaloriesBurnedPerRep();
            }
        }
        model.addAttribute("progressToCaloriesBurned", caloriesBurned);
        model.addAttribute("targetCaloriesBurned", activeUser.getTargetCalorieBurned());
        return "exercise-counter";
    }
}
