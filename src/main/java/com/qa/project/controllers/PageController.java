package com.qa.project.controllers;

import com.qa.project.persistence.model.*;
import com.qa.project.service.IngredientService;
import com.qa.project.service.MealHistoryService;
import com.qa.project.service.UserService;
import com.qa.project.validator.validators.SecQuestionHashmap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class PageController {
    @Value("${spring.application.name}")
    String appName;
    @Autowired
    UserService userService;

    @Autowired
    IngredientService ingredientService;

    private User getUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        return userService.findByUsername(username);
    }
    @GetMapping("/")
    public String homePage(Model model) {
        model.addAttribute("appName", appName);
        return "index";
    }
    @GetMapping({"/ingredient-table", "/ingredient-table/{date}"})
    public String ingredientTable(@PathVariable(value="date", required=false) String d, Model model) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        if(d == null){
            Date date = Calendar.getInstance().getTime();
            d = dateFormat.format(date);

        }
        model.addAttribute("date", d);
        return "ingredient-table";
    }

    @GetMapping({"/exercise-table", "/exercise-table/{date}"})
    public String exerciseTable(@PathVariable(value="date", required=false) String d, Model model) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        if(d == null){
            Date date = Calendar.getInstance().getTime();
            d = dateFormat.format(date);

        }
        model.addAttribute("date", d);
        return "exercise-table";
    }

    @GetMapping({"/changeIngredient", "/changeIngredient/{date}"})
    public String updateIngredientShowPage(@PathVariable(value="date", required=false) String d, Model model) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        if(d == null){
            Date date = Calendar.getInstance().getTime();
            d = dateFormat.format(date);

        }
        User activeUser = getUser();

        List<Ingredient> foundIngredients = activeUser.getOwnedIngredients();
        model.addAttribute("ingredients", foundIngredients);
        model.addAttribute("date", d);
        return "update-ingredient";
    }

    @GetMapping({"/changeExercise", "/changeExercise/{date}"})
    public String updateExerciseShowPage(@PathVariable(value="date", required=false) String d, Model model) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        if(d == null){
            Date date = Calendar.getInstance().getTime();
            d = dateFormat.format(date);

        }

        User activeUser = getUser();
        List<Exercise> foundExercises = activeUser.getOwnedExercises();
        model.addAttribute("exercises", foundExercises);
        model.addAttribute("date", d);
        return "update-exercise";
    }

    @GetMapping({"/delete-ingredient", "/delete-ingredient/{date}"})
    public String deleteIngredientShowPage(@PathVariable(value="date", required=false) String d, Model model) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        if(d == null){
            Date date = Calendar.getInstance().getTime();
            d = dateFormat.format(date);

        }

        User activeUser = getUser();
        List<Ingredient> foundIngredients = activeUser.getOwnedIngredients();
        model.addAttribute("ingredients", foundIngredients);
        model.addAttribute("date", d);
        return "delete-ingredient";
    }

    @GetMapping({"/delete-exercise", "/delete-exercise/{date}"})
    public String deleteExerciseShowPage(@PathVariable(value="date", required=false) String d, Model model) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        if(d == null){
            Date date = Calendar.getInstance().getTime();
            d = dateFormat.format(date);

        }

        User activeUser = getUser();
        List<Exercise> foundExercises = activeUser.getOwnedExercises();
        model.addAttribute("exercises", foundExercises);
        model.addAttribute("date", d);
        return "delete-exercise";
    }

    @GetMapping({"/meal-table", "/meal-table/{date}"})
    public String mealTable(@PathVariable(value="date", required=false) String d, Model model) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        if(d == null){
            Date date = Calendar.getInstance().getTime();
            d = dateFormat.format(date);

        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date= formatter.parse(d);
        User activeUser = getUser();
        TempMeal mealForm = new TempMeal();
        List<Ingredient> foundIngredients = activeUser.getOwnedIngredients();
        for(Ingredient i : foundIngredients){
            mealForm.addIngredient(i);
        }
        Boolean found = false;
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
        for(MealHistory m : activeUser.getMealHistories()){
            String testDate = m.getDate().toString();
            testDate = testDate.substring(0,10);
            if(formatter2.parse(testDate).equals(date)){
                found = true;
                for(Ingredient i : m.getMealHistoryIngredient()){
                    mealForm.addSelected(i);
                }
            }
        }
        if(mealForm.getSelected().size() <10){
            mealForm.addSelected(new Ingredient());
        }
        model.addAttribute("form", mealForm);
        model.addAttribute("date", dateFormat.format(date));
        return "meal-table";
    }

    @GetMapping({"/routine-table", "/routine-table/{date}"})
    public String routineTable(@PathVariable(value="date", required=false) String d, Model model) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        if(d == null){
            Date date = Calendar.getInstance().getTime();
            d = dateFormat.format(date);

        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date= formatter.parse(d);
        User activeUser = getUser();
        TempRoutine routineForm = new TempRoutine();
        List<Exercise> foundExercises = activeUser.getOwnedExercises();
        for(Exercise e : foundExercises){
            routineForm.addExercise(e);
        }
        Boolean found = false;
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
        for(RoutineHistory r : activeUser.getRoutineHistories()){
            String testDate = r.getDate().toString();
            testDate = testDate.substring(0,10);
            if(formatter2.parse(testDate).equals(date)){
                found = true;
                for(Exercise e : r.getRoutineHistoryExercise()){
                    routineForm.addSelected(e);
                }
            }
        }
        if(routineForm.getSelected().size() <10){
            routineForm.addSelected(new Exercise());
        }
        model.addAttribute("form", routineForm);
        model.addAttribute("date", dateFormat.format(date));
        return "routine-table";
    }

    @Autowired
    MealHistoryService mealHistoryService;


    @GetMapping("/signup")
    public String showSignUpForm(User user, Model model) {
        SecQuestionHashmap mappings = new SecQuestionHashmap();
        Set<String> secQuestions = new HashSet<String>();
        for(String i : mappings.getMap().keySet()){
            secQuestions.add(i);
        }
        model.addAttribute("secQuestions", secQuestions);
        return "add-user";
    }
    @GetMapping("/accDetails")
    public String accDetails(Model model) {
        User activeUser = getUser();
        model.addAttribute("email", activeUser.getEmail());
        model.addAttribute("username", activeUser.getUsername());
        return "accountSettings";
    }


    @GetMapping({"/passRecover"})
    public String passRecovery(User user, BindingResult result, Model model) {
        return "passRecover";
    }

    @GetMapping("/settings/{success}")
    public String settings(@PathVariable("success") Integer success, Model model) {
        User user = getUser();
        model.addAttribute("targetCalorieBurned", user.getTargetCalorieBurned());
        model.addAttribute("targetCalorieIntake", user.getTargetCalorieIntake());
        model.addAttribute("targetCarbIntake", user.getTargetCarbIntake());
        model.addAttribute("targetFatIntake", user.getTargetFatIntake());
        model.addAttribute("targetProteinIntake", user.getTargetProteinIntake());
        if(success == 1){
            model.addAttribute("message", "success");
        }
        return "settings";
    }

    @GetMapping({"/nutrition/dateUpdate"})
    public String nutritionUpdate(@RequestParam(required = false) String d, Model model) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        if(d == null){
            Date date = Calendar.getInstance().getTime();
            d = dateFormat.format(date);
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date= formatter.parse(d);
        DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
        model.addAttribute("date", dateFormat2.format(date));


        return "nutrition";
    }

    @GetMapping({"/training/dateUpdate"})
    public String trainingUpdate(@RequestParam(required = false) String d, Model model) throws ParseException {

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        if(d == null){
            Date date = Calendar.getInstance().getTime();
            d = dateFormat.format(date);
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date= formatter.parse(d);


        DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
        model.addAttribute("date", dateFormat2.format(date));


        return "training";
    }

    @GetMapping({"/nutrition", "/nutrition/{date}"})
    public String nutrition(@PathVariable(value="date", required=false) String d, Model model) throws ParseException {

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        if(d == null){

            Date date = Calendar.getInstance().getTime();
            d = dateFormat.format(date);

        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date= formatter.parse(d);
        List<Integer> targets = new ArrayList<Integer>();
        User activeUser = getUser();
        targets.add(activeUser.getTargetCalorieIntake());
        targets.add(activeUser.getTargetProteinIntake());
        targets.add(activeUser.getTargetCarbIntake());
        targets.add(activeUser.getTargetFatIntake());
        model.addAttribute("targets",targets);

        List<MealHistory> userMealHistory = activeUser.getMealHistories();
        Boolean found = false;
        MealHistory foundMealHistory = new MealHistory();
        for(MealHistory m : userMealHistory){
            if (dateFormat.format(m.getDate()).equals(d)){
                found = true;
                foundMealHistory = m;
            }
        }
        List<Integer> progressToTargets = new ArrayList<Integer>();
        int totalProtein = 0;
        int totalCarb = 0;
        int totalFat = 0;
        int totalCalorieIn = 0;
        if(found){
            for(Ingredient i : foundMealHistory.getMealHistoryIngredient()){
                totalCalorieIn += i.getCalories();
                totalFat+= i.getFats();
                totalCarb+= i.getCarbs();
                totalProtein += i.getProtein();
            }
        }

        progressToTargets.add(totalCalorieIn);
        progressToTargets.add(totalProtein);
        progressToTargets.add(totalCarb);
        progressToTargets.add(totalFat);

        model.addAttribute("progress", progressToTargets);
        DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
        model.addAttribute("date", dateFormat2.format(date));
        return "nutrition";
    }

    @RequestMapping(value="/{reader}", method= RequestMethod.GET)
    public String statics(Model model) {
        model.addAttribute("appName", appName);
        return "index";
    }

    @GetMapping({"/training", "/training/{date}"})
    public String training(@PathVariable(value="date", required=false) String d, Model model) throws ParseException {

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        if(d == null){

            Date date = Calendar.getInstance().getTime();
            d = dateFormat.format(date);

        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date= formatter.parse(d);

        User activeUser = getUser();
        Integer target = activeUser.getTargetCalorieBurned();

        model.addAttribute("target", target);

        List<RoutineHistory> userRoutineHistory = activeUser.getRoutineHistories();
        Boolean found = false;
        RoutineHistory foundRoutineHistory = new RoutineHistory();
        for(RoutineHistory r : userRoutineHistory){
            if (dateFormat.format(r.getDate()).equals(d)){
                found = true;
                foundRoutineHistory = r;
            }
        }
        Integer progressToTarget = 0;
        if(found){
            for(Exercise e : foundRoutineHistory.getRoutineHistoryExercise()){
                progressToTarget += e.getCaloriesBurnedPerRep();
            }
        }

        model.addAttribute("progress", progressToTarget);
        DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
        model.addAttribute("date", dateFormat2.format(date));
        return "training";
    }
    @GetMapping({"/home"})
    public String home(@RequestParam(required = false) String d, Model model) throws ParseException {
        User activeUser = getUser();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        if(d == null){

            Date date = Calendar.getInstance().getTime();
            d = dateFormat.format(date);

        }else{
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date= formatter.parse(d);
            d = dateFormat.format(date);
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date= formatter.parse(d);
        List<MealHistory> userMealHistory = activeUser.getMealHistories();
        Boolean found = false;
        MealHistory foundMealHistory = new MealHistory();
        for(MealHistory m : userMealHistory){
            if (dateFormat.format(m.getDate()).equals(d)){
                found = true;
                foundMealHistory = m;

            }
        }

        if(!found){
            MealHistory mealHistory = new MealHistory();
            mealHistory.setOwnedBy(activeUser);
            mealHistory.setDate(date);
            MealHistory newMealHistory = mealHistoryService.save(mealHistory);
            userMealHistory.add(newMealHistory);
            activeUser.setMealHistories(userMealHistory);
            userService.save(activeUser);
            for(MealHistory m : userMealHistory){
                if (dateFormat.format(m.getDate()).equals(d)){
                    found = true;
                    foundMealHistory = m;

                }
            }

        }
        List<Integer> progressToTargets = new ArrayList<Integer>();
        int totalProtein = 0;
        int totalCarb = 0;
        int totalFat = 0;
        int totalCalorieIn = 0;
        int totalCalorieOut = 0;
        if(found){
            for(Ingredient i : foundMealHistory.getMealHistoryIngredient()){
                totalCalorieIn += i.getCalories();
                totalFat+= i.getFats();
                totalCarb+= i.getCarbs();
                totalProtein += i.getProtein();
            }
        }

        progressToTargets.add(totalCalorieIn);
        progressToTargets.add(totalProtein);
        progressToTargets.add(totalCarb);
        progressToTargets.add(totalFat);

        List<Integer> targets = new ArrayList<Integer>();
        targets.add(activeUser.getTargetCalorieIntake());
        targets.add(activeUser.getTargetProteinIntake());
        targets.add(activeUser.getTargetCarbIntake());
        targets.add(activeUser.getTargetFatIntake());
        model.addAttribute("progress", progressToTargets);
        model.addAttribute("targets",targets);
        DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
        model.addAttribute("date", dateFormat2.format(date));



        userMealHistory.sort(new Comparator<MealHistory>() {

            public int compare(MealHistory o1, MealHistory o2) {
                return o2.getDate().compareTo(o1.getDate());
            }
        });
        String entries = "";
        String dates = "";
        int count = 0;
        if(found){
            int today = userMealHistory.indexOf(foundMealHistory);
            for(int i = today; i<userMealHistory.size(); i++){
                int calories = 0;
                for(Ingredient ingredient : userMealHistory.get(i).getMealHistoryIngredient()){
                    calories += ingredient.getCalories();
                }
                if(count != 0){
                    entries += ",";
                    dates += ",";
                }
                entries += (calories);
                dates+=dateFormat.format(userMealHistory.get(i).getDate());
                if(count == 50){
                    break;
                }
                count++;
            }
        }
        for(int i = count; i<20; i++){
            entries += ",0";
            dates += ",0";
        }
        model.addAttribute("dates", dates);
        model.addAttribute("calorieEntries", entries);

        List<RoutineHistory> userRoutineHistory = activeUser.getRoutineHistories();

        Boolean foundRoutine = false;
        RoutineHistory foundRoutineHistory = new RoutineHistory();
        for(RoutineHistory r : userRoutineHistory){
            if (dateFormat.format(r.getDate()).equals(d)){
                foundRoutine = true;
                foundRoutineHistory = r;

            }
        }
        userRoutineHistory.sort(new Comparator<RoutineHistory>() {

            public int compare(RoutineHistory o1, RoutineHistory o2) {
                return o2.getDate().compareTo(o1.getDate());
            }
        });
        String routineEntries = "";
        String routineDates = "";
        count = 0;
        List<String> todayGroups = new ArrayList<>();
        List<Integer> todayTotalGroupCount = new ArrayList<>();
        if(foundRoutine){
            for(Exercise e : foundRoutineHistory.getRoutineHistoryExercise()){
                if(todayGroups.contains(e.getMuscleGroupOne())){
                    int indexToIncrement = todayGroups.indexOf(e.getMuscleGroupOne());
                    todayTotalGroupCount.set(indexToIncrement, todayTotalGroupCount.get(indexToIncrement)+1);
                }else{
                    if(!e.getMuscleGroupOne().equals("")){
                        todayGroups.add(e.getMuscleGroupOne());
                        todayTotalGroupCount.add(1);
                    }

                }
                if(todayGroups.contains(e.getMuscleGroupTwo())){
                    int indexToIncrement = todayGroups.indexOf(e.getMuscleGroupTwo());
                    todayTotalGroupCount.set(indexToIncrement, todayTotalGroupCount.get(indexToIncrement)+1);
                }else{
                    if(!e.getMuscleGroupTwo().equals("")){
                        todayGroups.add(e.getMuscleGroupTwo());
                        todayTotalGroupCount.add(1);
                    }
                }
            }
        }
        String muscleEntries = todayGroups.toString();
        muscleEntries=muscleEntries.substring(1,muscleEntries.length()-1);
        String counts = todayTotalGroupCount.toString();
        counts=counts.substring(1,counts.length()-1);
        model.addAttribute("muscleGroupEntries", muscleEntries);
        model.addAttribute("muscleGroupCounts", counts);

        int caloriesBurned = 0;
        if(foundRoutine){
            for(Exercise e : foundRoutineHistory.getRoutineHistoryExercise()){
                caloriesBurned += e.getCaloriesBurnedPerRep();
            }
        }
        model.addAttribute("progressToCaloriesBurned", caloriesBurned);
        model.addAttribute("targetCaloriesBurned", activeUser.getTargetCalorieBurned());

        return "home";
    }

}