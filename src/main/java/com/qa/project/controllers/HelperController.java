package com.qa.project.controllers;

import com.qa.project.persistence.model.Role;
import com.qa.project.persistence.model.User;
import com.qa.project.persistence.repo.RoleRepository;
import com.qa.project.service.UserService;
import com.qa.project.validator.validators.SecQuestionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;

@Controller
@Configuration
@ComponentScan("com.qa.project.persistence")
@ComponentScan("com.qa.project.service")
public class HelperController {
	@Autowired
	private UserService userService;
	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	public void DataRoleLoader(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

	@Autowired
	public void DataUserLoader(UserService userService) {
		this.userService = userService;
		LoadUser();
	}



	private void LoadUser() {
		Role u = new Role();
		u.setName("ROLE_USER");

		Role a = new Role();
		a.setName("ROLE_ADMIN");

		roleRepository.save(u);
		roleRepository.save(a);

		User admin = new User("admin123", "Abc123456**", "Abc123456**", "james@gmail.com", "Abc123456", SecQuestionEnum.MAIDEN_NAME);
		admin.setRoles(roleRepository.findAll());

		User user = new User("user123", "Abc123456**", "Abc123456**", "james2@gmail.com", "Abc123456", SecQuestionEnum.MAIDEN_NAME);
		user.setRoles(roleRepository.findRoleByName("ROLE_USER"));

	}

}
