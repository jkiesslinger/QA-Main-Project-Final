package com.qa.project.controllers;

import com.qa.project.persistence.model.Exercise;
import com.qa.project.persistence.model.Ingredient;
import com.qa.project.persistence.model.MealHistory;
import com.qa.project.persistence.model.User;
import com.qa.project.service.IngredientService;
import com.qa.project.service.MealHistoryService;
import com.qa.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.qa.project.helper.Constants.*;

@Controller
public class IngredientController {
    @Autowired
    private IngredientService ingredientService;
    @Autowired
    private UserService userService;

    SimpleDateFormat formatter = new SimpleDateFormat(yearMonthDay);
    DateFormat dateFormat = new SimpleDateFormat(dayMonthYear);
    private String formatDateYearFirstToDayFirst(String date) throws ParseException {
        Date datePlaceholder = formatter.parse(date);
        return dateFormat.format(datePlaceholder);
    }
    private void saveIngredientToUser(Ingredient ingredientForm, User user){
        ingredientForm.setOwnedBy(user);
        Ingredient savedIngredient = ingredientService.save(ingredientForm);
        List<Ingredient> ingredients = user.getOwnedIngredients();
        ingredients.add(savedIngredient);
        user.setOwnedIngredients(ingredients);
        //try catch
        if(!savedIngredient.getId().equals(null)){
            userService.save(user);
        }
    }
    private User getUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            String username = ((UserDetails)principal).getUsername();
            return userService.findByUsername(username);
        } else {
            String username = principal.toString();
            return userService.findByUsername(username);
        }
    }
    @GetMapping({ingredientCreateMapping, ingredientCreateMappingWithDate})
    public String addIngredient(@PathVariable(value=pageDate, required=false) String date, @ModelAttribute("ingredientForm") Ingredient ingredientForm) throws ParseException {
        date = formatDateYearFirstToDayFirst(date);
        saveIngredientToUser(ingredientForm, getUser());
        if(date.equals(nullString)){
            return ingredientRedirect;
        }
        return ingredientRedirect+ date;

    }
    private void updateIngredientToUser(User user, Ingredient ingredientForm){
        ingredientForm.setOwnedBy(user);
        for(Ingredient i : user.getOwnedIngredients()){
            if(i.getId().equals(ingredientForm.getId())){
                ingredientForm.setName(i.getName());
                ingredientService.save(ingredientForm);
            }
        }
    }


    @GetMapping({ingredientUpdateMapping, ingredientUpdateMappingWithDate})
    public String updateIngredient(@PathVariable(value=pageDate, required=false) String d, @ModelAttribute("ingredientForm") Ingredient ingredientForm) {
        updateIngredientToUser(getUser(), ingredientForm);
        return ingredientRedirect + d;
    }

    @Autowired
    MealHistoryService mealHistoryService;


    private Boolean isOwnedByLoggedInUser(Ingredient toRemove, User user){
        return toRemove.getOwnedBy().equals(user);
    }

    @GetMapping({ingredientDeleteMapping, ingredientDeleteMappingWithDate})
    public String deleteIngredient(@PathVariable(value=pageDate, required=false) String d, @ModelAttribute("ingredientForm") Ingredient ingredientForm) {
        //try catch
        if(ingredientForm.getId()==null){
            return ingredientRedirect + d;
        }
        User user = getUser();
        Ingredient toRemove = ingredientService.getOneById(ingredientForm.getId());
        if(isOwnedByLoggedInUser(toRemove,user)){
            purgeIngredient(toRemove, user);
        }
        return ingredientRedirect + d;
    }
    private void purgeIngredientFromUser(Ingredient toRemove, User user){
        user.getOwnedIngredients().remove(toRemove);
        userService.save(user);
    }
    private List<MealHistory> getAllHistoriesContainingIngredient(Ingredient toRemove, User user){
        List<MealHistory> ingredientLocatedInMealHistories = new ArrayList<>();
        for(MealHistory mealHistory : user.getMealHistories()){
            for(Ingredient ingredient : mealHistory.getMealHistoryIngredient()){
                if(ingredient.getId().equals(toRemove.getId())){
                    ingredientLocatedInMealHistories.add(mealHistory);
                }
            }
        }
        return ingredientLocatedInMealHistories;
    }

    private void purgeIngredientFromHistories(Ingredient toRemove, User user){
        List<MealHistory> ingredientLocatedInMealHistories = getAllHistoriesContainingIngredient(toRemove, user);
        for(MealHistory m : ingredientLocatedInMealHistories){
            m.getMealHistoryIngredient().remove(toRemove);
            mealHistoryService.save(m);
        }
    }
    private void purgeIngredient(Ingredient toRemove, User user){
        purgeIngredientFromUser(toRemove, user);
        purgeIngredientFromHistories(toRemove, user);
        ingredientService.delete(toRemove.getId());
    }
}
