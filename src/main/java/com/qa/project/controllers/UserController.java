package com.qa.project.controllers;

import com.qa.project.persistence.model.User;
import com.qa.project.persistence.repo.RoleRepository;
import com.qa.project.service.UserService;
import com.qa.project.validator.validators.SecQuestionEnum;
import com.qa.project.validator.validators.SecQuestionHashmap;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.SecureRandom;
import java.util.*;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    SmartValidator validator;

    private final Random r = new Random();

    @GetMapping("/changeSettings")
    public String changeSettings(User user, BindingResult result, Model model) {

        User toChange = getUser();
        toChange.setTargetCalorieBurned(user.getTargetCalorieBurned());
        toChange.setTargetCalorieIntake(user.getTargetCalorieIntake());
        toChange.setTargetCarbIntake(user.getTargetCarbIntake());
        toChange.setTargetProteinIntake(user.getTargetProteinIntake());
        toChange.setTargetFatIntake(user.getTargetFatIntake());
        userService.save(toChange);
        return "redirect:/settings/1";
    }

    @PostMapping("/adduser")
    public String addUser(User user, BindingResult result, Model model) {

        validateAccount(user, result);
        if (result.hasErrors()) {
            addErrors(model, result);
            addSecQuestions(model);
            return "add-user";
        }else if(userService.findByEmail(user.getEmail()) != null){
            model.addAttribute("emailErrors", "This email is already taken");
            model.addAttribute("passRecover",1);
            return "add-user";
        }else if(userService.findByUsername(user.getUsername()) != null){
            model.addAttribute("usernameErrors", "This username is already taken");
            return "add-user";
        }
        remapSecQuestion(user);
        user.setRoles(roleRepository.findRoleByName("ROLE_USER"));
        userService.save(user);
        model.addAttribute("users", userService.getAll());
        return "login";
    }

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Bad Credentials.");
        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");
        return "login";
    }

    @PostMapping("/logout")
    public String logout(Model model, String error) {
        model.addAttribute("message", "You have been logged out successfully.");
        return "login";
    }

    @PostMapping("/recover")
    public String recover(User user, BindingResult result, Model model) {
        User found = userService.findByEmail(user.getEmail());
        if(user.getSecAnswer() != null){
            if(found != null){
                if(found.getSecAnswer().equals(user.getSecAnswer())){
                    String newPassword = genPassword();
                    found.setPassword(newPassword);
                    found.setPasswordConfirm(newPassword);
                    userService.save(found);
                    model.addAttribute("newPass", "Success! New password: " + newPassword);
                }else{
                    model.addAttribute("error", "Incorrect Details");
                }
            }else{
                model.addAttribute("error", "Incorrect Details");
            }

        }else{
            if(found != null){
                model.addAttribute("inputEmail", user.getEmail());
                String SecQ = "";
                SecQuestionHashmap mapping = new SecQuestionHashmap();
                HashMap<String, SecQuestionEnum> values= mapping.getMap();
                for(Map.Entry e : values.entrySet()){
                    if(e.getValue().equals(found.getSecQuestion())){
                        SecQ = e.getKey().toString();
                    }
                }
                model.addAttribute("secQuestion",SecQ);
            }else{
                model.addAttribute("error","Incorrect Details");
            }
        }
        return "passRecover";

    }

    @PostMapping("/changeAccountSettings")
    public String updateAccSettings(User user, BindingResult result, Model model) {
        final String accSettings = "accountSettings";
        User activeUser = getUser();
        activeUser.setUsername(user.getUsername());
        activeUser.setEmail(user.getEmail());
        activeUser.setPassword(user.getPassword());
        activeUser.setPasswordConfirm(user.getPasswordConfirm());


        if(user.getSecAnswer() != null){
            activeUser.setSecAnswer(user.getSecAnswer());
        }
        SecQuestionHashmap mappings = new SecQuestionHashmap();


        for(Map.Entry i : mappings.getMap().entrySet()){
            if(i.getValue().equals(activeUser.getSecQuestion())){
                activeUser.setSecQuestionString(i.getKey().toString());
            }
        }

        validateAccount(activeUser, result);

        if (result.hasErrors()) {
            addErrors(model, result);
            model.addAttribute("email", user.getEmail());
            model.addAttribute("username", user.getUsername());
            return accSettings;
        }else if(userService.findByEmail(user.getEmail()) != null){
            User testCase = userService.findByEmail(user.getEmail());
            if(!testCase.getId().equals(activeUser.getId())){

                model.addAttribute("emailErrors", "This email is already taken");
                model.addAttribute("passRecover",1);
                return accSettings;
            }

        }else if(userService.findByUsername(user.getUsername()) != null){
            User testCase = userService.findByUsername(user.getUsername());
            if(!testCase.getId().equals(activeUser.getId())){
                model.addAttribute("usernameErrors", "This username is already taken");
                return accSettings;
            }
        }
        userService.saveAndFlush(activeUser);
        User testUser = userService.findByUsername(activeUser.getUsername());
        model.addAttribute("message", "Success");
        model.addAttribute("email", user.getEmail());
        model.addAttribute("username", user.getUsername());
        return accSettings;
    }

    public void validateAccount(@ModelAttribute User account, BindingResult result) {
        validator.validate(account, result);
    }

    private User getUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        return userService.findByUsername(username);
    }

    private void addErrors(Model model, BindingResult result){
        List<String> passwordErrors = new ArrayList<>();
        List<String> passwordConfirmErrors = new ArrayList<>();
        List<String> emailErrors = new ArrayList<>();
        List<String> secAnswerErrors = new ArrayList<>();
        List<String> secQuestionErrors = new ArrayList<>();
        List<String> usernameErrors = new ArrayList<>();
        List<String> miscErrors = new ArrayList<>();
        for(Object object : result.getAllErrors()){
            if(object instanceof FieldError){
                FieldError e = (FieldError) object;
                String field = e.getField();
                switch (field){
                    case "password":
                        passwordErrors.add(e.getDefaultMessage());
                        break;
                    case "email":
                        emailErrors.add(e.getDefaultMessage());
                        break;
                    case "secAnswer":
                        secAnswerErrors.add(e.getDefaultMessage());
                        break;
                    case "secQuestionString":
                        secQuestionErrors.add(e.getDefaultMessage());
                        break;
                    case "username":
                        usernameErrors.add(e.getDefaultMessage());
                        break;
                }
            }else if(object instanceof ObjectError){
                ObjectError e = (ObjectError) object;
                passwordConfirmErrors.add(e.getDefaultMessage());
            }
        }
        model.addAttribute("passwordErrors", passwordErrors);
        model.addAttribute("passwordConfirmErrors", passwordConfirmErrors);
        model.addAttribute("emailErrors", emailErrors);
        model.addAttribute("secAnswerErrors", secAnswerErrors);
        model.addAttribute("secQuestionErrors", secQuestionErrors);
        model.addAttribute("usernameErrors", usernameErrors);
        model.addAttribute("miscErrors", miscErrors);
        int errCountInt = passwordErrors.size() + passwordConfirmErrors.size() + emailErrors.size() + secAnswerErrors.size() + secQuestionErrors.size() + usernameErrors.size() + miscErrors.size();
        String errCount = Integer.toString(errCountInt);
        model.addAttribute("errorCount", errCount);
    }

    private void addSecQuestions(Model model){
        SecQuestionHashmap mappings = new SecQuestionHashmap();
        Set<String> secQuestions = new HashSet<String>();
        for(String i : mappings.getMap().keySet()){
            secQuestions.add(i);
        }
        model.addAttribute("secQuestions", secQuestions);
    }

    private void remapSecQuestion(User user){
        SecQuestionHashmap mapping = new SecQuestionHashmap();
        user.setSecQuestion(mapping.getMap().get(user.getSecQuestionString()));
    }

    public String genPassword(){
        //CONSTANTS in constants file
        char[] possibleLower = ("abcdefghijklmnopqrstuvwxyz").toCharArray();
        char[] possibleUpper = ("ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
        char[] possibleNumber = ("1234567890").toCharArray();
        char[] possibleSpecial = ("~`!@#$%^&*()-_=+[{]}|;:,<.>/?").toCharArray();
        int max = 6;
        int min = 3;

        int count = r.nextInt(max - min + 1) + min;
        String randpass = RandomStringUtils.random( count, 0, possibleLower.length-1, false, false, possibleLower, new SecureRandom() );
        count = r.nextInt(max - min + 1) + min;
        randpass += RandomStringUtils.random( count, 0, possibleUpper.length-1, false, false, possibleUpper, new SecureRandom() );
        count = r.nextInt(max - min + 1) + min;
        randpass += RandomStringUtils.random( count, 0, possibleNumber.length-1, false, false, possibleNumber, new SecureRandom() );
        count = r.nextInt(max - min + 1) + min;
        randpass += RandomStringUtils.random( count, 0, possibleSpecial.length-1, false, false, possibleSpecial, new SecureRandom() );
        randpass = shuffleString(randpass);

        return randpass;
    }

    public static String shuffleString(String string)
    {
        List<String> letters = Arrays.asList(string.split(""));
        Collections.shuffle(letters);
        String shuffled = "";
        for (String letter : letters) {
            shuffled += letter;
        }
        return shuffled;
    }
}
