package com.qa.project.helper;


public class Constants {

    public static final String yearMonthDay = "yyyy-MM-dd";
    public static final String dayMonthYear = "dd-MM-yyyy";
    public static final String pageDate = "date";
    public static final String nullString = "null";


    public static final String exerciseDeleteMapping = "/exercise/delete";
    public static final String exerciseDeleteMappingWithDate = "/exercise/delete/{date}";
    public static final String exerciseUpdateMapping = "/exercise/update";
    public static final String exerciseUpdateMappingWithDate = "/exercise/update/{date}";
    public static final String exerciseCreateMapping = "/exercise/add";
    public static final String exerciseCreateMappingWithDate = "/exercise/add/{date}";

    public static final String ingredientDeleteMapping = "/ingredient/delete";
    public static final String ingredientDeleteMappingWithDate = "/ingredient/delete/{date}";
    public static final String ingredientUpdateMapping = "/ingredient/update";
    public static final String ingredientUpdateMappingWithDate = "/ingredient/update/{date}";
    public static final String ingredientCreateMapping = "/ingredient/add";
    public static final String ingredientCreateMappingWithDate = "/ingredient/add/{date}";

    public static final String exerciseRedirect = "redirect:/exercise-table/";
    public static final String ingredientRedirect = "redirect:/ingredient-table/";

}
