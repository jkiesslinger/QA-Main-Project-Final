package com.qa.project.persistence.repo;

import com.qa.project.persistence.model.MealHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MealHistoryRepository extends JpaRepository<MealHistory, Long>{
}
