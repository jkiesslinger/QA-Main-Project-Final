package com.qa.project.persistence.repo;

import com.qa.project.persistence.model.RoutineHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoutineHistoryRepository extends JpaRepository<RoutineHistory, Long>{
}
