package com.qa.project.persistence.model;

import javax.persistence.*;

@Entity
@Table(name = "ingredient")
public class Ingredient extends IngredientSuper{
	public Ingredient(String name, Integer calories, Integer protein, Integer carbs, Integer fats, String username) {
		this.calories = calories;
		this.fats = fats;
		this.protein = protein;
		this.carbs = carbs;
		this.name = name;
		this.username = username;
	}

	public Ingredient() {
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Transient
	String username;

    @ManyToOne
	private MealHistory mealHistory;
}
