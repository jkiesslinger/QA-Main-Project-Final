package com.qa.project.persistence.model;

import java.util.ArrayList;
import java.util.List;

//Wrapper
public class TempMeal {
    public TempMeal() {
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    private List<Ingredient> ingredients = new ArrayList<Ingredient>();

    public List<Ingredient> getSelected() {
        return selected;
    }

    public void setSelected(List<Ingredient> selected) {
        this.selected = selected;
    }

    private List<Ingredient> selected = new ArrayList<>();



    public List<Ingredient> getIngredients() {
        return ingredients;
    }
    public void removeNulls(){
        while (this.selected.remove(null));
    }


        public void addIngredient(Ingredient ingredientToAdd) {
        this.ingredients.add(ingredientToAdd);
    }
    public void addSelected(Ingredient i) {
        this.selected.add(i);
    }
    public void removeSelected(Ingredient i) {
        this.selected.remove(i);
    }
}
