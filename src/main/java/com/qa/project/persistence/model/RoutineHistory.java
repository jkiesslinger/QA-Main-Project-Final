package com.qa.project.persistence.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "routine_history")
public class RoutineHistory {
	public RoutineHistory() {
	}

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	private Date date;

	public User getOwnedBy() {
		return ownedBy;
	}

	public void setOwnedBy(User ownedBy) {
		this.ownedBy = ownedBy;
	}

	public List<Exercise> getRoutineHistoryExercise() {
		return routineHistoryExercise;
	}

	public void setRoutineHistoryExercise(List<Exercise> routineHistoryExercise) {
		this.routineHistoryExercise = routineHistoryExercise;
	}

	@ManyToOne
	private User ownedBy;

	@ManyToMany
	@JoinTable(
			name = "exercise_routine_history",
			joinColumns = @JoinColumn(name = "routine_history_id"),
			inverseJoinColumns = @JoinColumn(name = "exercise_id"))
	private List<Exercise> routineHistoryExercise;
}
