package com.qa.project.persistence.model;

import com.qa.project.validator.annotations.*;
import com.qa.project.validator.validators.SecQuestionEnum;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user")
@ValidPasswordMatches
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public User(@NotNull @NotEmpty String username, @NotNull @NotEmpty String password, @NotNull @NotEmpty String passwordConfirm, @NotNull @NotEmpty String email, @NotNull @NotEmpty String secAnswer, @NotNull @NotEmpty SecQuestionEnum secQuestion) {
        this.username = username;
        this.password = password;
        this.passwordConfirm = passwordConfirm;
        this.email = email;
        this.secAnswer = secAnswer;
        this.secQuestion = secQuestion;
    }

    public User() {
    }

    private Integer TargetCalorieBurned = 500;
    private Integer TargetCalorieIntake = 2000;
    private Integer TargetCarbIntake = 250;

    public Integer getTargetCalorieBurned() {
        return TargetCalorieBurned;
    }

    public void setTargetCalorieBurned(Integer targetCalorieBurned) {
        TargetCalorieBurned = targetCalorieBurned;
    }

    public Integer getTargetCalorieIntake() {
        return TargetCalorieIntake;
    }

    public void setTargetCalorieIntake(Integer targetCalorieIntake) {
        TargetCalorieIntake = targetCalorieIntake;
    }

    public Integer getTargetCarbIntake() {
        return TargetCarbIntake;
    }

    public void setTargetCarbIntake(Integer targetCarbIntake) {
        TargetCarbIntake = targetCarbIntake;
    }

    public Integer getTargetProteinIntake() {
        return TargetProteinIntake;
    }

    public void setTargetProteinIntake(Integer targetProteinIntake) {
        TargetProteinIntake = targetProteinIntake;
    }

    public Integer getTargetFatIntake() {
        return TargetFatIntake;
    }

    public void setTargetFatIntake(Integer targetFatIntake) {
        TargetFatIntake = targetFatIntake;
    }

    private Integer TargetProteinIntake = 130;
    private Integer TargetFatIntake = 60;

    @ValidUsername6To32Char
    @ValidUsernameHasNoSpecialChar
    @NotNull
    @NotEmpty
    private String username;

    @ValidPasswordHasNumber
    @ValidPassword8to32Char
    @ValidPasswordHasLowercase
    @ValidPasswordHasSpecialChar
    @ValidPasswordHasUppercase
    @NotNull
    @NotEmpty
    private String password;

    @Transient
    @NotNull
    @NotEmpty
    private String passwordConfirm;

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @ManyToMany
    private List<Role> roles;

    public List<Ingredient> getOwnedIngredients() {
        if(ownedIngredients == null){
            return new ArrayList<Ingredient>();

        }
        return ownedIngredients;
    }

    public List<Exercise> getOwnedExercises() {
        return ownedExercises;
    }

    public void setOwnedIngredients(List<Ingredient> ownedIngredients) {
        this.ownedIngredients = ownedIngredients;
    }

    public void setOwnedExercises(List<Exercise> ownedExercises) {
        this.ownedExercises = ownedExercises;
    }

    @OneToMany
    protected List<Ingredient> ownedIngredients = new ArrayList<>();

    @OneToMany
    protected List<Exercise> ownedExercises = new ArrayList<>();

    @ValidEmail
    @NotNull
    @NotEmpty
    private String email;

    public List<MealHistory> getMealHistories() {
        return mealHistories;
    }

    public List<RoutineHistory> getRoutineHistories() {
        return routineHistories;
    }

    public void setMealHistories(List<MealHistory> mealHistories) {
        this.mealHistories = mealHistories;
    }

    public void setRoutineHistories(List<RoutineHistory> routineHistories) {
        this.routineHistories = routineHistories;
    }

    @OneToMany
    protected List<MealHistory> mealHistories = new ArrayList<>();

    @OneToMany
    protected List<RoutineHistory> routineHistories = new ArrayList<>();

    @ValidSecAnswer
    @NotNull
    @NotEmpty
    private String secAnswer;

    public String getSecQuestionString() {
        return secQuestionString;
    }

    public void setSecQuestionString(String secQuestionString) {
        this.secQuestionString = secQuestionString;
    }

    @Transient
    @ValidSecQuestion
    private String secQuestionString;

    private SecQuestionEnum secQuestion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSecAnswer() {
		return secAnswer;
	}

	public void setSecAnswer(String secAnswer) {
		this.secAnswer = secAnswer;
	}

	public SecQuestionEnum getSecQuestion() {
		return secQuestion;
	}

	public void setSecQuestion(SecQuestionEnum secQuestion) {
		this.secQuestion = secQuestion;
	}
}
