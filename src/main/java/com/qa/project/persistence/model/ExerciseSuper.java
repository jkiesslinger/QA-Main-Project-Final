package com.qa.project.persistence.model;

import javax.persistence.*;

@MappedSuperclass
public class ExerciseSuper {
    public int getIdAsInt() {
        if(getId() == null){
            return -1;
        }
        return id.intValue();
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    protected String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getOwnedBy() {
        return ownedBy;
    }

    public void setOwnedBy(User ownedBy) {
        this.ownedBy = ownedBy;
    }

    protected Integer caloriesBurnedPerRep;

    protected String muscleGroupOne;

    public Integer getCaloriesBurnedPerRep() {
        return caloriesBurnedPerRep;
    }

    public void setCaloriesBurnedPerRep(Integer caloriesBurnedPerRep) {
        this.caloriesBurnedPerRep = caloriesBurnedPerRep;
    }

    public String getMuscleGroupOne() {
        return muscleGroupOne;
    }

    public void setMuscleGroupOne(String muscleGroupOne) {
        this.muscleGroupOne = muscleGroupOne;
    }

    public String getMuscleGroupTwo() {
        return muscleGroupTwo;
    }

    public void setMuscleGroupTwo(String muscleGroupTwo) {
        this.muscleGroupTwo = muscleGroupTwo;
    }

    protected String muscleGroupTwo;

    @ManyToOne
    protected User ownedBy;
}
