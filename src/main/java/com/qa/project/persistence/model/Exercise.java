package com.qa.project.persistence.model;

import javax.persistence.*;

@Entity
@Table(name = "exercise")
public class Exercise extends ExerciseSuper{
	public Exercise(String name, Integer caloriesBurnedPerRep, String muscleGroupOne, String muscleGroupTwo, String username) {
		this.caloriesBurnedPerRep = caloriesBurnedPerRep;
		this.muscleGroupOne = muscleGroupOne;
		this.muscleGroupTwo = muscleGroupTwo;
		this.name = name;
		this.username = username;
	}

	public Exercise() {
	}

	@Transient
	String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@ManyToOne
	private RoutineHistory routineHistory;
}
