package com.qa.project.persistence.model;

import javax.persistence.*;

@MappedSuperclass
public class IngredientSuper {
	public int getIdAsInt() {
		if(getId() == null){
			return -1;
		}
		return id.intValue();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

	protected String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getOwnedBy() {
		return ownedBy;
	}

	public void setOwnedBy(User ownedBy) {
		this.ownedBy = ownedBy;
	}

	public Integer getCalories() {
		return calories;
	}

	public void setCalories(Integer calories) {
		this.calories = calories;
	}

	public Integer getProtein() {
		return protein;
	}

	public void setProtein(Integer protein) {
		this.protein = protein;
	}

	public Integer getCarbs() {
		return carbs;
	}

	public void setCarbs(Integer carbs) {
		this.carbs = carbs;
	}

	public Integer getFats() {
		return fats;
	}

	public void setFats(Integer fats) {
		this.fats = fats;
	}

	@ManyToOne
	protected User ownedBy;

	protected Integer calories;

	protected Integer protein;

	protected Integer carbs;

	protected Integer fats;
}
