package com.qa.project.persistence.model;

import java.util.ArrayList;
import java.util.List;

//Wrapper
public class TempRoutine {
    public TempRoutine() {
    }

    public void setExercises(List<Exercise> exercises) {
        this.exercises = exercises;
    }

    private List<Exercise> exercises = new ArrayList<Exercise>();

    public List<Exercise> getSelected() {
        return selected;
    }

    public void setSelected(List<Exercise> selected) {
        this.selected = selected;
    }

    private List<Exercise> selected = new ArrayList<>();



    public List<Exercise> getExercises() {
        return exercises;
    }
    public void removeNulls(){
        while (this.selected.remove(null));
    }


        public void addExercise(Exercise exerciseToAdd) {
        this.exercises.add(exerciseToAdd);
    }
    public void addSelected(Exercise e) {
        this.selected.add(e);
    }
    public void removeSelected(Exercise e) {
        this.selected.remove(e);
    }
}
