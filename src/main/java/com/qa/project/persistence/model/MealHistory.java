package com.qa.project.persistence.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "meal_history")
public class MealHistory {

	public MealHistory() {
	}

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	private Date date;

	public User getOwnedBy() {
		return ownedBy;
	}

	public void setOwnedBy(User ownedBy) {
		this.ownedBy = ownedBy;
	}

	public List<Ingredient> getMealHistoryIngredient() {
		if(mealHistoryIngredient == null){
			List<Ingredient> dummyList = new ArrayList<Ingredient>();
			return dummyList;
		}
		return mealHistoryIngredient;
	}

	public void setMealHistoryIngredient(List<Ingredient> mealHistoryIngredient) {
		this.mealHistoryIngredient = mealHistoryIngredient;
	}

	@ManyToOne
	private User ownedBy;
	
	@ManyToMany
    @JoinTable(
    		  name = "ingredient_meal_history", 
    		  joinColumns = @JoinColumn(name = "meal_history_id"), 
    		  inverseJoinColumns = @JoinColumn(name = "ingredient_id"))
    private List<Ingredient> mealHistoryIngredient;
	
}
