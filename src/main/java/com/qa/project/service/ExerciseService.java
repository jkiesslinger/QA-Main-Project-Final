package com.qa.project.service;

import com.qa.project.persistence.model.Exercise;

import java.util.List;

public interface ExerciseService{
    Exercise save(Exercise exercise);
    List<Exercise> getAll();
    Exercise getOneById(Long id);
    Void delete(Long id);
	
}
