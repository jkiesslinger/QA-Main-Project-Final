package com.qa.project.service;

import com.qa.project.persistence.model.MealHistory;

public interface MealHistoryService {

	MealHistory save(MealHistory mealHistory);

	Iterable<MealHistory> getAll();
}
