package com.qa.project.service;

import com.qa.project.persistence.model.RoutineHistory;

public interface RoutineHistoryService {
	void save(RoutineHistory routineHistory);

	Iterable<RoutineHistory> getAll();
}
