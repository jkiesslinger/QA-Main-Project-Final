package com.qa.project.service;

import com.qa.project.persistence.model.Ingredient;

import java.util.List;

public interface IngredientService{
    Ingredient save(Ingredient ingredient);
	List<Ingredient> getAll();
	Ingredient getOneById(Long id);
	Void delete(Long id);
	
}
