package com.qa.project.service.serviceImpl;

import com.qa.project.persistence.model.RoutineHistory;
import com.qa.project.persistence.repo.RoutineHistoryRepository;
import com.qa.project.service.RoutineHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoutineHistoryServiceImpl implements RoutineHistoryService {
    @Autowired
    private RoutineHistoryRepository routineHistoryRepository;

    @Override
    public void save(RoutineHistory routineHistory) {
    	routineHistoryRepository.save(routineHistory);
    }

	@Override
	public Iterable<RoutineHistory> getAll() {
		return routineHistoryRepository.findAll();
	}

	
}
