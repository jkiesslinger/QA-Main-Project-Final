package com.qa.project.service.serviceImpl;

import com.qa.project.persistence.model.Ingredient;
import com.qa.project.persistence.repo.IngredientRepository;
import com.qa.project.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientServiceImpl implements IngredientService {
    @Autowired
    private IngredientRepository ingredientRepository;

    @Override
    public Ingredient save(Ingredient ingredient) {
        if(ingredient.getCalories() == null){
            ingredient.setCalories(0);
        }
        if(ingredient.getCarbs() == null){
            ingredient.setCarbs(0);
        }
        if(ingredient.getProtein() == null){
            ingredient.setProtein(0);
        }
        if(ingredient.getFats() == null){
            ingredient.setFats(0);
        }
    	if(ingredient.getName() != null && !ingredient.getName().equals("")){
            return ingredientRepository.save(ingredient);
        }
        return ingredient;
    }

	@Override
	public List<Ingredient> getAll() {
		return ingredientRepository.findAll();
	}

    @Override
    public Ingredient getOneById(Long id) {
        return ingredientRepository.getOne(id);
    }

    @Override
    public Void delete(Long id) {
        ingredientRepository.delete(ingredientRepository.getOne(id));
        return null;
    }


}
