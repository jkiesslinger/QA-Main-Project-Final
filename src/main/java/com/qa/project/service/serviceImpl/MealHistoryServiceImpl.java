package com.qa.project.service.serviceImpl;

import com.qa.project.persistence.model.MealHistory;
import com.qa.project.persistence.repo.MealHistoryRepository;
import com.qa.project.service.MealHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MealHistoryServiceImpl implements MealHistoryService {
    @Autowired
    private MealHistoryRepository repo;

    @Override
    public MealHistory save(MealHistory mealHistory) {
    	return repo.save(mealHistory);
    }

	@Override
	public Iterable<MealHistory> getAll() {
		return repo.findAll();
	}
}

	