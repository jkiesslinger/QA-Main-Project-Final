package com.qa.project.service.serviceImpl;

import com.qa.project.persistence.model.Exercise;
import com.qa.project.persistence.repo.ExerciseRepository;
import com.qa.project.service.ExerciseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExerciseServiceImpl implements ExerciseService {
    @Autowired
    private ExerciseRepository exerciseRepository;

    @Override
    public Exercise save(Exercise exercise) {
        if(exercise.getCaloriesBurnedPerRep() == null)
            exercise.setCaloriesBurnedPerRep(0);
        if(exercise.getName() != null && !exercise.getName().equals("")){
            return exerciseRepository.save(exercise);
        }
        return exercise;
    }

    @Override
    public List<Exercise> getAll() {
        return exerciseRepository.findAll();
    }

    @Override
    public Exercise getOneById(Long id) {
        return exerciseRepository.getOne(id);
    }

    @Override
    public Void delete(Long id) {
        exerciseRepository.delete(exerciseRepository.getOne(id));
        return null;
    }

	
}
