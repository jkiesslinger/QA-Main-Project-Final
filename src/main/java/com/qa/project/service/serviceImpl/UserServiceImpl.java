package com.qa.project.service.serviceImpl;


import com.qa.project.persistence.model.User;
import com.qa.project.persistence.repo.UserRepository;
import com.qa.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

	@Override
	public User findByEmail(String email) {

		return userRepository.findByEmail(email);
	}

	@Override
    public void saveAndFlush(User u){
        u.setPassword(bCryptPasswordEncoder.encode(u.getPassword()));
        userRepository.saveAndFlush(u);
    }
    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

}
