package com.qa.project.service;

import com.qa.project.persistence.model.User;

import java.util.List;

public interface UserService{
    void save(User user);

    User findByUsername(String username);

	User findByEmail(String username);
	void saveAndFlush(User u);
	List<User> getAll();
}
